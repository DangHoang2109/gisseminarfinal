const express = require('express')
const cors = require("cors");
const xlsx = require('xlsx')
const fs = require('fs')
const jsonData = require('./PopulationData.json')

const app = express();
const port = process.env.PORT || 8000;

app.use(express.static('./'));
app.use(express.json({ 
  limit: '50mb'
}));

app.use(cors());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/WebMaster.html')
})

app.get('/test', (req, res) => {
  res.sendFile(__dirname + '/DrawMap.html')
})

app.post('/save', (req,res) => {
  console.log("Body: ", req.body)
  try {
    fs.writeFile(req.body.filePath, JSON.stringify(req.body.geoData),() => console.log("Done"))
  } catch(e) {
    console.log(e)
  }
  res.send(200)
})

app.get('/process', (req,res) => {
  const workbook = xlsx.readFile('./Database Mật độ.xlsx')
  res.send(workbook)

  const data = workbook["Sheets"]["V02.01"]
  const ProvinceColID = "A"
  const AreaColID = "B"
  const PopulateColID = "C"
  const year = "2011"
  var index = 5

  var obj = jsonData['document'][year.toString()]["province"]

    for (var item in obj) {

      //console.log(data[PopulateColID + index.toString()])
      obj[item]['Số dân'] = data[PopulateColID + index.toString()]["v"]

      obj[item]['Diện tích'] = data[AreaColID + index.toString()]["v"]

      index = index + 1
    }

    fs.writeFile("PopulationData.json", JSON.stringify(jsonData),() => console.log("Done"))

})

app.listen(port, () => {
  console.log(`Server is up in port + ${port} 🐳`);
});